﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace A1 {
    public partial class Form1 : Form {
        // the connection to the database
        SqlConnection sqlConnection;
        // extracts data from database using a sql query and the connection to it to fill the dataset and updates the database
        SqlDataAdapter parentDataAdapter, childDataAdapter;
        // contains the tables, relations, etc. of our database in memory
        DataSet dataSet;
        // automatically creates insert, update, delete
        // department command builder is not necessarry as we don't have to change department (parent) rows
        SqlCommandBuilder childCommandBuilder;
        // binds the grid views with the dataset (gui to database)
        // the child will bind to the parent in order to show the members correspoding to the parent
        BindingSource parentBindingSource, childBindingSource;

        String parentTableName = ConfigurationManager.AppSettings.Get("parentTableName");
        String parentRelationColumnName = ConfigurationManager.AppSettings.Get("parentRelationColumnName");
        String parentQuery = ConfigurationManager.AppSettings.Get("parentQuery");

        String childTableName = ConfigurationManager.AppSettings.Get("childTableName");
        String childRelationColumnName = ConfigurationManager.AppSettings.Get("childRelationColumnName");
        String childQuery = ConfigurationManager.AppSettings.Get("childQuery");

        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            // database connection
            sqlConnection = new SqlConnection("Data Source = DESKTOP-CDITB1E\\SQLEXPRESS; " +
                "Initial Catalog = DBMS Practical Exam; " +
                "Integrated Security = SSPI;");

            // data fetching
            parentTableName = "TaskType";
            parentRelationColumnName = "id";
            parentQuery = "select * from " + parentTableName;

            childTableName = "Task";
            childRelationColumnName = "task_type_id";
            childQuery = "select * from " + childTableName;

            dataSet = new DataSet();

            parentDataAdapter = new SqlDataAdapter(parentQuery, sqlConnection);
            childDataAdapter = new SqlDataAdapter(childQuery, sqlConnection);

            childCommandBuilder = new SqlCommandBuilder(childDataAdapter);

            parentDataAdapter.Fill(dataSet, parentTableName);
            childDataAdapter.Fill(dataSet, childTableName);

            DataRelation dataRelation = new DataRelation("fk_" + parentTableName + childTableName,
                dataSet.Tables[parentTableName].Columns[parentRelationColumnName],
                dataSet.Tables[childTableName].Columns[childRelationColumnName]);
            dataSet.Relations.Add(dataRelation);

            // DataGridView's binding
            parentBindingSource = new BindingSource();
            parentBindingSource.DataSource = dataSet;
            parentBindingSource.DataMember = parentTableName;

            childBindingSource = new BindingSource(parentBindingSource, "fk_" + parentTableName + childTableName);

            dgvTaskTypes.DataSource = parentBindingSource;
            dgvTasks.DataSource = childBindingSource;

            label1.Text = parentTableName;
            label2.Text = childTableName;
        }

        private void refreshDatabase_Click(object sender, EventArgs e) {
            // updates database and refreshes the child table
            childDataAdapter.Update(dataSet, childTableName);
            dataSet.Tables[childTableName].Clear();
            childDataAdapter.Fill(dataSet, childTableName);
        }
    }
}
