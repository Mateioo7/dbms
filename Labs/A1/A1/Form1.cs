﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace A1 {
    public partial class Form1 : Form {
        // the connection to the database
        SqlConnection sqlConnection;
        // extracts data from database using a sql query and the connection to it to fill the dataset and updates the database
        SqlDataAdapter departmentDataAdapter, animalDataAdapter;
        // contains the tables, relations, etc. of our database in memory
        DataSet dataSet;
        // automatically creates insert, update, delete
        // department command builder is not necessarry as we don't have to change department (parent) rows
        SqlCommandBuilder animalCommandBuilder;
        // binds the grid views with the dataset (gui to database)
        // the child will bind to the parent in order to show the members correspoding to the parent
        BindingSource departmentBindingSource, animalBindingSource;

        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            sqlConnection = new SqlConnection("Data Source = DESKTOP-CDITB1E\\SQLEXPRESS; " +
                "Initial Catalog = Zoo; " +
                "Integrated Security = SSPI;");
            dataSet = new DataSet();

            departmentDataAdapter = new SqlDataAdapter("select * from Department", sqlConnection);
            animalDataAdapter = new SqlDataAdapter("select * from Animal", sqlConnection);

            animalCommandBuilder = new SqlCommandBuilder(animalDataAdapter);

            departmentDataAdapter.Fill(dataSet, "Departments");
            animalDataAdapter.Fill(dataSet, "Animals");

            DataRelation dataRelation = new DataRelation("fk_Departments_Animals",
                dataSet.Tables["Departments"].Columns["DId"],
                dataSet.Tables["Animals"].Columns["DId"]);
            dataSet.Relations.Add(dataRelation);

            departmentBindingSource = new BindingSource();
            departmentBindingSource.DataSource = dataSet;
            departmentBindingSource.DataMember = "Departments";

            animalBindingSource = new BindingSource(departmentBindingSource, "fk_Departments_Animals");

            departmentsDataGridView.DataSource = departmentBindingSource;
            animalsDataGridView.DataSource = animalBindingSource;
        }

        private void updateDatabase_Click(object sender, EventArgs e) {
            animalDataAdapter.Update(dataSet, "Animals");
        }
    }
}
