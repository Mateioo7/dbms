﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
// SqlConnection, SqlCommand, SqlDataReader, SqlDataAdapter

//SqlCommand: ExecuteScalar, ExecuteReader, ExecuteNonQuery

namespace L1 {
    class Program {
        static void Main(string[] args) {
            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = "Data Source = DESKTOP-CDITB1E\\SQLEXPRESS; " +
                "Initial Catalog = Zoo; " +
                "Integrated Security = SSPI";
            sqlConnection.Open();

            SqlCommand sqlCommand1 = new SqlCommand("select count(*) from Animal", sqlConnection);
            String result1 = sqlCommand1.ExecuteScalar().ToString();
            Console.WriteLine(result1);

            SqlCommand sqlCommand2 = new SqlCommand("select * from Animal", sqlConnection);
            using (SqlDataReader dataReader = sqlCommand2.ExecuteReader()) {
                while (dataReader.Read()) {
                    Console.WriteLine("{0}, {1}", dataReader["AId"], dataReader["Name"]);
                }
            }

            /*SqlCommand sqlCommand3 = new SqlCommand("insert into Animal(AId, Name) values " +
                "(999999, 'Gica') ", sqlConnection);
            sqlCommand3.ExecuteNonQuery();*/

            result1 = sqlCommand1.ExecuteScalar().ToString();
            Console.WriteLine(result1);

            sqlConnection.Close();

            Console.WriteLine("\nDone. Press enter.");
            Console.ReadLine();



            /*DataSet dataSet = new DataSet();
            SqlDataAdapter dataAdapter = new SqlDataAdapter("select * from Animal", sqlConnection);
            dataAdapter.Fill(dataSet, "Animal");
            

            foreach (DataRow dataRow in dataSet.Tables["Animal"].Rows) {
                Console.WriteLine("{0}, {1}", dataRow["AId"], dataRow["Name"]);
            }

            DataRow newDataRow = dataSet.Tables["Animal"].NewRow();
            newDataRow["AId"] = 9999999;
            newDataRow["Name"] = "Gica";

            dataSet.Tables["Animal"].Rows.Add(newDataRow);*/



        }
    }
}
