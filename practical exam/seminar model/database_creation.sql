use [DBMS Practical Exam];

create table Category (
	id int primary key,
	description varchar(200)
)

create table Page (
	id int primary key,
	name varchar(50),
	category_id int foreign key references Category(id)
)

create table Users (
	id int primary key,
	city varchar(50),
	date_of_birth date
)

create table Likes (
	user_id int references Users(id),
	page_id int references Page(id),
	date date,
	primary key (user_id, page_id)
)

create table Post (
	id int primary key,
	user_id int foreign key references Users(id),
	text varchar(200),
	date date,
	number_of_shares int
)

create table Comment (
	id int primary key,
	post_id int foreign key references Post(id),
	text varchar(200),
	date date,
	is_top_comment bit
)

insert into Users values 
	(1, 'Zalau', '2000-05-23'),
	(2, 'Cluj-Napoca', '1999-02-12')

insert into Post values 
	(1, 1, 'Buna ziua, clan', '2000-05-23', 5),
	(2, 1, 'Ce meci', '1999-02-12', 8),
	(3, 2, 'Azi v-am pregatit o reteta speciala', '1996-10-25', 12)
