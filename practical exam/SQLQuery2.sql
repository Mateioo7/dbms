use [DBMS Practical Exam];

DBCC useroptions;
-- issue:
set transaction isolation level READ COMMITTED;
-- solution:
set transaction isolation level REPEATABLE READ;

-- explanation:
-- read committed level - shared lock released immediately
-- order: query 1 (select) => query 3 (update) => query 2 (select)

-- repeatable read level - shared lock released at the end of transaction
-- order: query 1 (select) => query 2 (select) => query 3 (update)

update TaskType
	set name = 'technical'
where name = 'bug';
-- make sure you run the above update before

begin transaction
	-- query 1
	select * from TaskType
		where name = 'technical';

	waitfor delay '00:00:06';

	-- query 2
	select * from TaskType
		where name = 'technical';
commit transaction