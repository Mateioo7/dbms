use [DBMS Practical Exam];

create table TaskType (
	id int primary key,
	name varchar(50),
	description varchar(200)
)

create table TaskPriority (
	id int primary key,
	name varchar(50),
	description varchar(200)
)

create table Developer (
	id int primary key,
	first_name varchar(20),
	last_name varchar(20),
)

create table Project (
	id int primary key,
	start_date date,
	end_date date
)

create table Task (
	id int primary key,
	developer_id int foreign key references Developer(id),
	project_id int foreign key references Project(id),
	task_type_id int foreign key references TaskType(id),
	task_priority_id int foreign key references TaskPriority(id),
	status varchar(20),
	title varchar(50),
	description varchar(200),
)

create table DevelopersProjects (
	developer_id int references Developer(id),
	project_id int references Project(id),
	primary key (developer_id, project_id)
)


insert into TaskType values (1, 'technical', 'typedescription1'), (2, 'bug', 'typedescription2')

insert into TaskPriority values (1, 'critical', 'description1'), (2, 'minor', 'description2')

insert into Developer values (1, 'Matei', 'Chis')

insert into Project values (1, '2020-05-23', '2021-05-23')

insert into Task values (1, 1, 1, 1, 1, 'in progress', 'task1', 'description1')

insert into Task values (2, 1, 1, 2, 1, 'closed', 'task2', 'description2')
